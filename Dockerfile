FROM hub.paas.kjtyun.com/library/centos7_jdk:12.0.2
#FROM hub.gcloud.lab/library/centos7_jdk:8u191

LABEL maintainer="wenzhihuai@globalegrow.com"


WORKDIR /data/work

RUN cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

ADD target/demo-0.0.1-SNAPSHOT.jar /data/work/demo-0.0.1-SNAPSHOT.jar

CMD ["java","-jar","demo-0.0.1-SNAPSHOT.jar"]

EXPOSE 8080